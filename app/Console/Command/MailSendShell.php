<?php
class MailSendShell extends AppShell {
    public $uses = array('Mail','Shop');

    public function main() {
        $this->autoRender = false;
	    $now = date("Y-m-d H:i:s");

        $query = "SELECT * FROM mails WHERE send_date >= '$now'
                                      AND del_flg != 1
                                      AND status != 1";

        $datas = $this->Mail->query($query);
        foreach($datas as $data){
            $from    = $data['mails']['from'];
            $subject = $data['mails']['subject'];
            $body    = $data['mails']['body'];
            $from = mb_convert_encoding($from, "SJIS","UTF-8");
            $subject = mb_convert_encoding($subject, "SJIS","UTF-8");
            $body = mb_convert_encoding($body, "SJIS","UTF-8");

            mb_language("japanese");
            mb_internal_encoding("SJIS");

            //全データに対して送信
            $params = array('conditions'=>array('del_flg'=>'0'));
            $shop_datas = $this->Shop->find($params);


            //html mail
            if($data['mails']['mail_type']){
                foreach($shop_datas as $shop_data){
                    //メール送信
                    $to = $shop_data['Shop']['email'];
                    $to = mb_convert_encoding($to, "SJIS","UTF-8");
                    $header_info="From: ".$to."\nContent-Type: text/plain;charset=ISO-2022-JP\nX-Mailer: PHP/".phpversion();
                    mb_send_mail($to,$subject,$body,"From:".$from,$header_info);
                }
            }
            else{//text mail
                //全データに対して送信
                foreach($shop_datas as $shop_data){
                    //メール送信
                    $to = $shop_data['Shop']['email'];
                    $to = mb_convert_encoding($to, "SJIS","UTF-8");
                    mb_send_mail($to,$subject,$body,"From:".$from);
                }
            }
            $rec_data['id'] = $data['mails']['id'];
            $rec_data['status'] = 1;
            $this->Shop->save($rec_data);
        }
	}
}
