<?php
App::uses('ShopsController', 'Controller');

/**
 * ShopsController Test Case
 *
 */
class ShopsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.shop'
	);

}
