<?php
App::uses('CodesController', 'Controller');

/**
 * CodesController Test Case
 *
 */
class CodesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.code',
		'app.shop'
	);

}
