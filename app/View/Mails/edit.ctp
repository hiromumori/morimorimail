<div class="mails form">
<?php echo $this->Form->create('Mail'); ?>
	<fieldset>
		<legend><?php echo __('Edit Mail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
        echo $this->Form->input('from');
		echo $this->Form->input('subject');
		echo $this->Form->input('body');
		echo $this->Form->input('send_date', array('type' => 'datetime', 'label' => '日時', 'dateFormat' => 'YMD', 'timeFormat' => '24', 'monthNames' => false, 'empty' => false, 'interval' => 15, 'minYear' => 2012));
		//echo $this->Form->input('status');
		//echo $this->Form->input('del_flg');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Mail'), array('action' => 'add')); ?></li>

	</ul>
    <h3><?php echo __('Menu'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link(__('Shop Data'), array('controller'=>'shops','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Send Mail'), array('controller'=>'mails','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('LOGOUT'), array('controller'=>'admins','action' => 'logout')); ?></li>
	</ul>
</div>
