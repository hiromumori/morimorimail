<div class="shops form">
<?php echo $this->Form->create('Shop'); ?>
	<fieldset>
		<legend><?php echo __('Add Shop'); ?></legend>
	<?php
		echo $this->Form->input('code_id');
		echo $this->Form->input('adrs');
		echo $this->Form->input('zip');
		echo $this->Form->input('tel');
		echo $this->Form->input('fax');
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('del_flg');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Shop'), array('action' => 'add')); ?></li>

	</ul>
    <h3><?php echo __('Menu'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link(__('Shop Data'), array('controller'=>'shops','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Send Mail'), array('controller'=>'mails','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('LOGOUT'), array('controller'=>'admins','action' => 'logout')); ?></li>
	</ul>
</div>
