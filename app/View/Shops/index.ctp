<div class="shops index">
	<h2><?php echo __('Shops'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('code_id'); ?></th>
			<th><?php echo $this->Paginator->sort('adrs'); ?></th>
			<th><?php echo $this->Paginator->sort('zip'); ?></th>
			<th><?php echo $this->Paginator->sort('tel'); ?></th>
			<th><?php echo $this->Paginator->sort('fax'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>

			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($shops as $shop): ?>
	<tr>
		<td><?php echo h($shop['Shop']['id']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['code_id']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['adrs']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['zip']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['tel']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['fax']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['name']); ?>&nbsp;</td>
		<td><?php echo h($shop['Shop']['email']); ?>&nbsp;</td>

		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $shop['Shop']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $shop['Shop']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $shop['Shop']['id']), null, __('Are you sure you want to delete # %s?', $shop['Shop']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Shop'), array('action' => 'add')); ?></li>

	</ul>
    <h3><?php echo __('Menu'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link(__('Shop Data'), array('controller'=>'shops','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Send Mail'), array('controller'=>'mails','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('LOGOUT'), array('controller'=>'admins','action' => 'logout')); ?></li>
	</ul>
</div>
