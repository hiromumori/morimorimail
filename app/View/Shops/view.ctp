<div class="shops view">
<h2><?php echo __('Shop'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code Id'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['code_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adrs'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['adrs']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['fax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Del Flg'); ?></dt>
		<dd>
			<?php echo h($shop['Shop']['del_flg']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Shop'), array('action' => 'add')); ?></li>

	</ul>
    <h3><?php echo __('Menu'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link(__('Shop Data'), array('controller'=>'shops','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Send Mail'), array('controller'=>'mails','action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('LOGOUT'), array('controller'=>'admins','action' => 'logout')); ?></li>
	</ul>
</div>
