<div id="login">
	<form name="loginform" id="loginform" action="<?php echo $this->Html->url(array('action' => 'login')); ?>" method="post">
		<p>
			<label for="user_login">ユーザー名
				<br />
				<input type="text" name="user"  value="" size="20" tabindex="10" />
			</label>
		</p>
		<p>
			<label for="user_pass">パスワード
				<br />
				<input type="password" name="pass"  value="" size="20" tabindex="20" />
			</label>
		</p>
		<p class="submit">
			<input type="submit" name="wp-submit" value="ログイン" tabindex="100" />
		</p>
	</form>
	<?php if(!empty($error_message)){ ?>
	<div class="note">
		<span style="color:#FF0000;"><?php echo $error_message; ?></span>
	</div>
	<?php } ?>
</div>
<div class="clear"></div>
