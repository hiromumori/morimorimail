<?php
App::uses('AppController', 'Controller');
/**
 * Mails Controller
 *
 * @property Mail $Mail
 * @property PaginatorComponent $Paginator
 */
class MailsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Mail->recursive = 0;
		$this->set('mails', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mail->exists($id)) {
			throw new NotFoundException(__('Invalid mail'));
		}
		$options = array('conditions' => array('Mail.' . $this->Mail->primaryKey => $id));
		$this->set('mail', $this->Mail->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mail->create();
            $rec_data = $this->request->data;
            $rec_data['Mail']['status'] = 0;
            $rec_data['Mail']['del_flg'] = 0;
			if ($this->Mail->save($rec_data)) {
				$this->Session->setFlash(__('The mail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Mail->exists($id)) {
			throw new NotFoundException(__('Invalid mail'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Mail->save($this->request->data)) {
				$this->Session->setFlash(__('The mail has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The mail could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Mail.' . $this->Mail->primaryKey => $id));
			$this->request->data = $this->Mail->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mail->id = $id;
		if (!$this->Mail->exists()) {
			throw new NotFoundException(__('Invalid mail'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Mail->delete()) {
			$this->Session->setFlash(__('The mail has been deleted.'));
		} else {
			$this->Session->setFlash(__('The mail could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
