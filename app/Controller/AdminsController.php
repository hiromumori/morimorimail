<?php
App::uses('AppController', 'Controller');
/**
 * Admins Controller
 *
 * @property Admin $Admin
 * @property PaginatorComponent $Paginator
 */
class AdminsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

    //ログイン認証
	public function login(){
		if(!empty($this->request->data)){
			$error_message = null;
			$LoginUser = $this->request->data['user'];
			$LoginPass = $this->request->data['pass'];

			if(empty($LoginUser) || empty($LoginPass)){
				$error_message = "IDとパスワードを入力して下さい。";
				$this->set("error_message",$error_message);
			}
			else{
				$user = $this->Admin->findByLoginId($LoginUser);
				if (!empty($user)) {
					$password = $user['Admin']['login_pass'];
					if ($password === $LoginPass) {
						//認証OK
						$this->Session->write('login_id', $user['Admin']['id']);
						$url = array("controller"=>"shops","action"=>"index");
						$this->redirect($url);
						exit;
					}
					else{
						//認証NG
						$error_message =  'IDかパスワードが違います。';
						$this->set("error_message",$error_message);
					}
				}
			}
		}
	}

	//ログインチェック
	public function logincheck(){
		$login_name = $this->Session->read('login_id');
		if(empty($login_name)){
			return FALSE;
		}
		return TRUE;
	}

	//ログアウト
	public function logout(){
		$this->Session->delete('login_id');
		$this->redirect("login");
		return;
	}

}
