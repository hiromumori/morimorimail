<?php
App::uses('AppModel', 'Model');
/**
 * Shop Model
 *
 */
class Shop extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
